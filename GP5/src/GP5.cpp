//============================================================================
// Name        : a.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

// GRUPO DE PROBLEMAS 5

#include<iostream>
using namespace std;

// PROB 1
/*int contador(int vector[],int n ,int k){
 int apa=0;
 for(int i=0;i<n;i++){
 apa=0;
 if(k==0){apa=n;}
 else{
 for(int i=0;i<n;i++){
 if((vector[i]==k) || (vector[i]%k==0)){
 apa++;
 }
 }
 }
 }

 return apa;
 }
 //prueba de la funcion
 int main(){
 int vector[]={1,2,2,4,5,7};
 int k=2;
 int apariciones;
 apariciones=contador(vector,6,k);
 cout<<apariciones;
 }*/


// PROB 2
/*int buscarValor(int vector[],int n,int k){
	int valor=-1;
	for(int i=0;(valor==-1);i++){
		if(vector[i]==k){
			valor=i;
		}
	}
return valor;

}

//prueba de la funcion
int main() {
	int vector[] = { 2, 3, 3, 4, 5, 6 };
	int k = 4;
	int resultado;
	resultado = buscarValor(vector, 6, k);
	cout << resultado;
	}*/


// PROB 3
/*void buscarValor(int vector[], int n, int a) {
	int k = 0;
	int indice;
	for (int i = 0; i < n; i++) {
		if (k == 0) {
			if (vector[i] == a) {
				indice = i;
				k = 1;
			}

			if ((k == 0) && (i == n - 1)) {
				indice = -1;
			}
		}
	}
if (indice==-1){cout<<"el valor no se encuentra en el vector";}
else{cout<<"el valor si se encuentra en el vector y su posicion es "<<indice;}

}

 //prueba de la funcion
 int main() {
 	int vector[] = { 2, 3, 3, 4, 5, 6 };
 	int k = 4;
         buscarValor(vector, 6, k);}*/



// PROB 4
//asumiendo vector creciente
/*int buscarValor(int vector[], int a) {
int indice = -1;
for(int i=0;vector[i]<=a;i++){
	if(vector[i]==a){
		indice=i;
	}
}
return indice;
}
 //prueba de la funcion
 int main() {
 	int vector[] = { 2, 3, 3, 4, 5, 6 };
 	int k = 4;
 	int resultado;
 	resultado = buscarValor(vector, k);
 	cout << resultado;
 	}*/



// PROB 5
/*int buscarValor(int vector[],int n,int a){
	int Ei=0;
	int Es=n;
	int M;
	int valor=-1;
	int comp=0;
	while(Ei<=Es){comp++;
		M=(Ei+Es)/2;
		if(vector[M]==a){
			valor=M;
		break;
		}
		if(vector[M]>a){
			Es=M;
		}
		if(vector[M]<a){
			Ei=M;
		}
	}
	cout<<"numero de comparaciones: "<<comp;
return valor;
}

 //prueba de la funcion
 int main() {
 	int vector[] = { 2, 3, 4, 5, 6 };
 	int k = 3;
 	int resultado;
 	resultado = buscarValor(vector, 5 ,k);
 	cout << resultado;
 	}*/



// PROB 6
/*// comparemos cada caso aumentador un contador de comparaciones
//reescribiendo las funciones anteriores

// caso 1 busqueda secuencial
long long BVsec(long long vector[],long long n,long long k){
	long long comp=0;
	int valor=-1;
	for(int i=0;(valor==-1);i++){
		if(vector[i]==k){
			valor=i;
		}
		comp++;
	}
return comp;
}

// caso 2 busqueda secuencial ordenada
long long BVsecOr(long long vector[], long long a) {
int indice = -1;
long long comp = 0;
for(int i=0;vector[i]<=a;i++){
	if(vector[i]==a){
		indice=i;
	}
	comp++;
}
return comp;
}

//caso 3 busqueda binaria

long long BVbin(long long vector[],long long n,long long a){
	int Ei=0;
	int Es=n;
	int M;
	int valor=-1;
	long long comp=0;
	while(Ei<=Es){comp++;
		M=(Ei+Es)/2;
		if(vector[M]==a){
			valor=M;
		}
		break;
		if(vector[M]>a){
			Es=M;
		}
		if(vector[M]<a){
			Ei=M;
		}
	}
return comp;
}

//usemos un vector cualquiera
int main (){
	cout<<"COMPAREMOS "<<endl;
	long long n;
	cout<<"digite el numero de elemntos del vector: ";
	cin>>n;
	long long vector[n];
	for(int i=0;i<n;i++){
		cout<<"digite el elemento "<<i+1<<": ";cin>>vector[i];
	}
	long long k;
	cout<<"digite el numero quisiera buscar: ";cin>>k;
	//ahora tenemos todo para iniciar a comparar
 long long Rs,Rso,Rb;
  Rs=BVsec(vector,n,k);
  Rso=BVsecOr(vector,k);
  Rb=BVbin(vector,n,k);
  // entonces tenemos
  cout<<"comparaciones por la busqueda secuencial: "<<Rs<<endl;
  cout<<"comparaciones por la busqueda secuencial ordenada: "<<Rso<<endl;
  cout<<"comparaciones por la busqueda binaria: "<<Rb<<endl;

// con ellos concluimos despues de varias comparaciones que
//la relacion de comparaciones de la busqueda secuencial tiene una forma de "O(N)"
 * siendo N el tama�o del vector y se entiendo como tiempo lineal proporcional al algoritmo
 * la busqueda secuencial tambien tiene la misma forma pero es mas efeciente "O(N)"
//la relacion de comparaciones de la busqueda binaria tiene una forma de "O(logN)"
 * siendo N el tama�o del vector y se entiendo como tiempo logaritmico proporcional al algoritmo
// conclusion el mas efectivo es la busqueda binaria aunque sirve en casos muy puntuales
 return 0;
}
*/

// PROB 7

/*void tienda(long V1[],long V2[],int n){
	long VV=0;
	for(int i=0;i<n;i++){
		VV=VV+V1[i]*V2[i];
	}
	cout<<"el subtotal es: "<<VV<<endl;
	int impuesto=VV*0.18;
	cout<<"el impuesto es: "<<impuesto<<endl;
	long PV=VV*1.18;
	if(PV>120){
		PV=PV*0.9;
	}
	cout<<"el monto total es: "<<PV<<endl;
}

// prueba de la funcion
int main() {
long V1[]={2,3,1,4,5,1,1,2};
long V2[]={3,4,6,1,1,2,8,10};
tienda(V1,V2,8);
return 0;
}

*/
// PROB 8

//robot []={U,A.D.I]
//          0 1 2 3

char contrario(char k){
	char contr;
	if(k=='A'){contr='U';}
	if(k=='U'){contr='A';}
	if(k=='D'){contr='I';}
	if(k=='I'){contr='D';}

	return contr;
}

int comandos(char comand[],int N){
	int bandera=0;
	char aux;
	for(int i=0;i<N;i++){
		aux=contrario(comand[i]);
		if(N-i>4){
			if(comand[i]==comand[i+1] && comand[i+2]==aux && comand[i+3]==aux && comand[i+4]==aux){
				bandera=1;break;
			}
		}
	}
	return bandera;
}

int main(){
char comando[]={'I','U','A','D','D','I','I'};
int resultado=comandos(comando,7);
cout<<resultado;

	return 0;
}

// PROB 9

